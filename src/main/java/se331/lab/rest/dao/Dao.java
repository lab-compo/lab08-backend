package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.List;
@Slf4j
@Profile("MyDao")
@Repository
public class Dao implements StudentDao {

    @Override
    public List<Student> getStudents() {
        log.info("My dao is called");
        return null;

    }

    @Override
    public Student getStudent(Long id) {
        return null;
    }

    @Override
    public Student saveStudent(Student student) {
        return null;
    }
}
